package com.retail.com;

public class classItem {
	
	 
	long productCode;
	 String description;
	 double price;
	 double weight;
	 String shippingType;
	
	
	 
	                   
	


		public classItem(long productCode, String description, double price, double weight, String shippingType)      
	     {                                                                 
	         this.productCode = productCode;
	         this.description = description;
	         this.price = price;
	         this.weight = weight;
	         this.shippingType = shippingType; 
	     }                                                                 
	
	      
	      public void setProductCode(long productCode)
	      {
	         this.productCode = productCode;
	      }
	
     
	      public long getProductCode()
	      {
	         return productCode;
	      }
	      
	      public void setDescription(String description)
	      {
	         this.description = description;
	      }
	
     
	      public String getDescription()
	      {
	         return description;
	      }
	      
	      
	      public void setPrice(double price)
	      {
	         this.price = price;
	      }
	
     
	      public double getPrice()
	      {
	         return price;
	      }
	      
	      public void setWeight(double weight)
	      {
	         this.weight = weight;
	      }
	
     
	      public double getWeight()
	      {
	         return weight;
	      }
	      
	      public String getShippingType() {
	  		return shippingType;
	  	  }


	  	  public void setShippingType(String shippingType) {
	  		this.shippingType = shippingType;
	  	  }
	      
	    
		public double calculate() {
			// TODO For air
			
			
			if(shippingType == "AIR") {
				long temp = (long) (productCode/10);
				int a = (int)(temp%10);
				double airShippingCost = a*weight;
				double perc = (3.00/100.00)*price ; 
				double total = airShippingCost+perc;
				//System.out.println("Total for "+ productCode + " is " + total);
				return total;
			}
			
			else
			{
			// For Ground
			
			double groundShippingCost = 2.5*weight;
			double perc2 = (3.00/100.00)*price ;
			double total2 = groundShippingCost+perc2;
			
			//System.out.println("Total for "+ productCode + " is " + total2);
			
			return total2;
			}
			
			
			
		}

}
