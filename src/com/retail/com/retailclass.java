/**
 * 
 */
package com.retail.com;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * @author masagi
 *
 */
public class retailclass {
	
	public static void main(String[] args)
	       {   
		
				
				classItem classItem1 = new classItem(567321101987L, "CD � Pink Floyd, Dark Side Of The Moon", 19.99, 0.58, "AIR");
				classItem1.calculate();
				classItem classItem2 = new classItem(567321101986L, "CD � Beatles, Abbey Road", 17.99, 0.61,"GROUND");
				classItem2.calculate();
				classItem classItem3 = new classItem(567321101985L, "CD � Queen, A Night at the Opera", 20.49, 0.55,"AIR");
				classItem3.calculate();
				classItem classItem4 = new classItem(567321101984L, "CD � Michael Jackson, Thriller", 23.88,0.50,"GROUND");
				classItem4.calculate();
				classItem classItem5 = new classItem(467321101899L, "iPhone - Waterproof Case", 9.75,0.73,"AIR");
				classItem5.calculate();
				classItem classItem6 = new classItem(477321101878L, "iPhone -  Headphones", 17.25,3.21,"GROUND");
				classItem6.calculate();
				
				
				//System.out.printf("%10s %30s %20s %5s %5s", "STUDENT ID", "EMAIL ID", "NAME", "AGE", "GRADE");
				System.out.printf("%-1s %27s %36s %12s %16s %-15s","UPC","Description","Price","weight","Ship Method","Shipping Cost");
				
				ArrayList<classItem> newItem = new ArrayList<classItem>(); 
				newItem.add(classItem1);
				newItem.add(classItem2);
				newItem.add(classItem3);
				newItem.add(classItem4);
				newItem.add(classItem5);
				newItem.add(classItem6);
				
				Iterator it = newItem.iterator();
				
				while(it.hasNext()) {
					classItem classitem = (classItem) it.next();
					double totalfinal = classitem.calculate();
					System.out.println();
					System.out.format("%1d %-42s %9f %10f %14s %-15f",classitem.getProductCode(),classitem.getDescription(), classitem.getPrice(),classitem.getWeight(),classitem.shippingType,totalfinal);
					
				}
				
				
				
				//System.out.printf(classItem1.getProductCode()+"\t"+classitem.getDescription()+"\t " + classItem1.getPrice()+"\t\t"+classItem1.getWeight());
	      }
}
